# Libre Governance



## Governace Documents Repository for Libre Blockchain

This repository holds all foundational documents for the Libre Blockchain. These are intended to provide a transparent set of common rules that will guide all users in interactions with the network. 

## The Documents

Libre has three foundational governance documents, the Libra Blockchain Operating Agreement (LBOA), which applies to all users and the Block Producers Minimum Requirements and Block Producers Regproducer agreements which only apply to those accounts that register as block producer candidates.

## Status of Documents

These documents are currently proposals only, modified from the Telos governance documents to suit the specific needs of the Libre Blockchain. Currently, neither these documents, nor any others have any validity in defining the accepted rules and regulations for operations of the Libre Blockchain. The Libre Governance Workgroup has been asked by the current group of Libre block producers to prepare these initial documents for discussion and eventual ratification.

## Amending These Documents

As there are as yet, no ratified governance documents for the Libre Blockchain, the first process required of the Libre block producers will be to modify these documents or others proposed for that purpose to be accepted as the Libre governance documents and ratify them by an on-chain multi-signature vote of the block producers. Once this occurs the method and requirements for further amending the documents will be described within the ratified governace documents. Proposals may be recorded in this repository but will only become effective by the ratification of a majority of the elected Libre block producers.

## Contributing

This open source repository welcomes any Libre user to submit Issues or Merge Requests. 

## License
The documents contained within this repository are public domain.
